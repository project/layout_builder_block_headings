<?php

namespace Drupal\Tests\layout_builder_block_headings\Functional;

use Drupal\block_content\Entity\BlockContent;
use Drupal\block_content\Entity\BlockContentType;
use Drupal\layout_builder\Entity\LayoutBuilderEntityViewDisplay;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;

/**
 * Test cases for the custom block headings feature.
 *
 * @group layout_builder_block_headings
 */
class LayoutBuilderBlockHeadingsTest extends BrowserTestBase {

  use ContentTypeCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'block_content',
    'contextual',
    'layout_builder',
    'block',
    'node',
    'options',
    'text',
    'layout_builder_block_headings',
    'layout_builder_block_headings_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->createContentType(['type' => 'test_type']);

    // Enable layout builder for the test type.
    /** @var \Drupal\layout_builder\Entity\LayoutEntityDisplayInterface $entity_display */
    $entity_display = LayoutBuilderEntityViewDisplay::load('node.test_type.default');
    $entity_display->enableLayoutBuilder()
      ->setOverridable()
      ->save();

    $this->drupalLogin($this->drupalCreateUser([], NULL, TRUE));
  }

  /**
   * Tests custom heading functionality.
   */
  public function testCustomHeadings() {

    $assert = $this->assertSession();

    // Ensure the third party settings are rendering properly.
    $path = (float)\Drupal::VERSION < 10 ?
      '/admin/structure/block/block-content/manage/test_custom_block_type' :
      '/admin/structure/block-content/manage/test_custom_block_type';

    $this->drupalGet($path);

    $heading_field = $assert->fieldExists('Heading field');
    static::assertEmpty($heading_field->getValue());

    $heading_level_field = $assert->fieldExists('Heading level field');
    static::assertEmpty($heading_level_field->getValue());

    $allow_heading_level_overrides = $assert->fieldExists('Allow the heading level field to be customized when added to a layout');
    static::assertFalse($allow_heading_level_overrides->isChecked());

    $heading_style_field = $assert->fieldExists('Heading style field');
    static::assertEmpty($heading_style_field->getValue());

    $allow_heading_style_overrides = $assert->fieldExists('Allow the heading style field to be customized when added to a layout');
    static::assertFalse($allow_heading_style_overrides->isChecked());

    $heading_field->selectOption('Test heading');

    // Create a reusable block instance.
    BlockContent::create([
      'type' => 'test_custom_block_type',
      'info' => 'Test reusable block',
      'field_test_heading' => [
        'value' => 'Test heading (reusable)',
        'format' => 'plain_text',
      ],
      'field_test_heading_level' => 'h3',
      'field_test_heading_style' => 'heading--small',
      'reusable' => TRUE,
    ])->save();

    // Enter the layout builder form.
    $this->drupalGet('/node/add/test_type');

    $this->submitForm([
      'Title' => 'Test node',
    ], 'Save');

    $this->drupalGet('/node/1/layout');

    // Add a reusable block to the layout.
    $this->drupalGet('/node/1/layout');
    $this->clickLink('Add block');
    $this->clickLink('Test reusable block');

    // By default, nothing is configured for the block type, so the behavior
    // should be exactly the same as out of the box core except for the
    // "administrative label" text override.
    $assert->fieldExists('Administrative label');
    $assert->fieldNotExists('Heading');
    $assert->fieldNotExists('Customize heading level');
    $assert->fieldNotExists('Heading level');
    $assert->fieldNotExists('Customize heading style');
    $assert->fieldNotExists('Heading style');

    $this->submitForm([], 'Add block');

    // The reusable block should not render a heading (yet), as its type
    // has not been configured.
    $reusable_block = $assert->elementExists('css', '[data-debug-block-label="Test reusable block"]');
    $assert->elementNotExists('css', '[class^="heading"]', $reusable_block);

    // Add an inline block to the layout.
    $this->clickLink('Add block');

    $button = (float)\Drupal::VERSION < 10.1 ?
      'Create custom block' :
      'Create content block';
    $this->clickLink($button);

    // Make sure our re-labelling of the 'info' field still works.
    $assert->fieldExists('Administrative label');

    $admin_label = $assert->fieldExists('Administrative label');
    $admin_label->setValue('Test inline block');
    $heading_field = $assert->fieldExists('Test heading');
    $heading_level_field = $assert->fieldExists('Test heading level');
    $heading_style_field = $assert->fieldExists('Test heading style');

    $heading_field->setValue('Test heading (inline)');
    $heading_level_field->selectOption('h4');
    $heading_style_field->selectOption('heading--large');

    $this->submitForm([], 'Add block');

    // The inline block should not render a heading (yet), as its type
    // has not been configured.
    $inline_block = $assert->elementExists('css', '[data-debug-block-label="Test inline block"]');
    $assert->elementNotExists('css', '[class^="heading"]', $inline_block);

    $this->submitForm([], 'Save layout');

    // Time to configure field mapping.
    $this->drupalGet('/admin/structure/block/block-content/manage/test_custom_block_type');
    $this->submitForm([
      'heading_field' => 'field_test_heading',
      'heading_level_field' => 'field_test_heading_level',
      'heading_style_field' => 'field_test_heading_style',
    ], 'Save');

    $bundle = BlockContentType::load('test_custom_block_type');
    static::assertEquals([
      'allow_heading_level_customization' => FALSE,
      'allow_heading_style_customization' => FALSE,
      'heading_field' => 'field_test_heading',
      'heading_level_field' => 'field_test_heading_level',
      'heading_style_field' => 'field_test_heading_style',
    ], $bundle->getThirdPartySettings('layout_builder_block_headings'));

    // Let's go check our blocks now, they should now both have headings.
    $this->drupalGet('/node/1');

    $reusable_block = $assert->elementExists('css', '[data-debug-block-label="Test reusable block"]');
    $reusable_block_heading = $assert->elementExists('css', '[class^="heading"]', $reusable_block);
    static::assertSame('Test heading (reusable)', $reusable_block_heading->getText());
    static::assertSame('h3', $reusable_block_heading->getTagName());
    static::assertStringContainsString('heading--small', $reusable_block_heading->getAttribute('class'));

    $inline_block = $assert->elementExists('css', '[data-debug-block-label="Test inline block"]');
    $inline_block_heading = $assert->elementExists('css', '[class^="heading"]', $inline_block);
    static::assertSame('Test heading (inline)', $inline_block_heading->getText());
    static::assertSame('h4', $inline_block_heading->getTagName());
    static::assertStringContainsString('heading--large', $inline_block_heading->getAttribute('class'));

    // Okay...now to allow overrides for the reusable block type.
    $this->drupalGet('/admin/structure/block/block-content/manage/test_custom_block_type');

    $assert->fieldExists('Allow the heading level field to be customized when added to a layout')->check();
    $assert->fieldExists('Allow the heading style field to be customized when added to a layout')->check();
    $this->submitForm([], 'Save');

    \Drupal::entityTypeManager()->getStorage('block_content_type')->resetCache();
    $bundle = BlockContentType::load('test_custom_block_type');
    static::assertEquals([
      'allow_heading_level_customization' => TRUE,
      'allow_heading_style_customization' => TRUE,
      'heading_field' => 'field_test_heading',
      'heading_level_field' => 'field_test_heading_level',
      'heading_style_field' => 'field_test_heading_style',
    ], $bundle->getThirdPartySettings('layout_builder_block_headings'));

    // Set up some overrides for the reusable block.
    $this->drupalGet('/node/1/layout');

    $this->submitForm([], 'Revert to defaults');
    $this->submitForm([], 'Revert');
    $this->drupalGet('/node/1/layout');

    $this->clickLink('Add block');
    $this->clickLink('Test reusable block');

    // Add a reusable block instance with overrides.
    $assert->fieldExists('Administrative label');
    $heading = $assert->fieldExists('Heading');
    static::assertSame('Test heading (reusable)', $heading->getValue());
    static::assertTrue($heading->hasAttribute('disabled'));

    $customize_heading_level = $assert->fieldExists('Customize heading level');
    $customize_heading_level->check();
    $heading_level = $assert->fieldExists('Heading level');
    $heading_level->selectOption('h2');
    $customize_heading_style = $assert->fieldExists('Customize heading style');
    $customize_heading_style->check();
    $heading_style = $assert->fieldExists('Heading style');
    $heading_style->selectOption('heading--large');
    $this->submitForm([], 'Add block');

    // We should now see a heading level override rendered for this block.
    $reusable_block = $assert->elementExists('css', '[data-debug-block-label="Test reusable block"]');
    $reusable_block_heading = $assert->elementExists('css', '[class^="heading"]', $reusable_block);
    static::assertSame('Test heading (reusable)', $reusable_block_heading->getText());
    static::assertSame('h2', $reusable_block_heading->getTagName());
    static::assertStringContainsString('heading--large', $reusable_block_heading->getAttribute('class'));

  }

}
